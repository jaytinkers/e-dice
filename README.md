# E-Dice Project
This is the repository for the design files of the E-Dice. For more information, checkout the associated blog on [JayTinkers.com](https://jaytinkers.com)

## Cloning
```
git clone git@gitlab.com:git@gitlab.com:jaytinkers/e-dice.git
```

## Dependency
- **jt-symbols-footprints**: rev1
  - Managed as a Git subtree, not a Git submodule. Therefore, it comes with the initial clone. There is nothing more to do.
  - Available at [jt_symbols_footprints](https://gitlab.com/jaytinkers/jt-symbols-footprints).

## Folder structure
The project design files are structured in the following folders
- **circuit_boards/bottom**: KiCad Project for the bottom PCB. Gerbers are found within.
- **circuit_boards/display**: KiCad Project for the top/display PCB. Gerbers are found within.
- **circuit_boards/libraries/custom**: Local KiCad library shared between the KiCAD projects. This is used for symbols and footprints specific to this project.
- **circuit_boards/libraries/jt_symbols_footprints**: [jt_symbols_footprints](https://gitlab.com/jaytinkers/jt-symbols-footprints) library. This is a global library with symbols and footprints for various Jay Tinkers projects. This is a Git subtree and points to the required revision.
- **software/attiny202/**: Firmware source for the ATTiny202 microcontroller on the bottom board. Production files are found within.
- **doc**: Various documentation including datasheets, user manuals and application notes.
