/*
 * E-Dice Finite State Machine
 */

#ifndef EDICE_FSM_H
#define EDICE_FSM_H

/*
 * Enumeration defining the 4 states of the E-Dice
 */
typedef enum {
    INIT,               // Initialization state (only after power-up)
    SLEEP,              // Power down state
    RNG,                // Random number generation update state
    SEQUENCE,           // Display the sequence state
    DISPLAY             // Display the random number generation state
} eState;

/*
 * Enumeration defining the possible events of the E-Dice
 */
typedef enum {
    NONE,               // No event
    SHOOK,              // E-Dice was shaken
    TIMEOUT            // Timeout (2s or 5s)
} eEvent;

/*
 * Global variable for the E-Dice state and Events
 * - States are updated by the finite state Machine
 * - Events are update by interrupts (thus we declare the variable volatile)
 */
extern eState ediceState;
extern eState prevEdiceState;
extern volatile eEvent ediceEvent;

/*
 * Finite State Machine for the E-Dice
 *
 * - This function never returns. It is an infinite loop.
 * - It updates the state of the E-Dice based on events that occur
 * - Events all originiate from interrupt
 */
void edice_fsm(void);

void edice_clear_event(void);

#endif
