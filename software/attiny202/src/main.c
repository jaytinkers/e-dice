#include "avr_fuse.h"
#include "edice_fsm.h"
#include "edice_actions.h"

 /*
  * Main function
  */
int main (void) {

    // Initialization phase after bootup
    edice_init();

    // Main loop, updatin the E-Dice state machine
    while(1) {
        edice_fsm();
    }

    // We should never reach this point
    return (0);
}
