#include <xc.h>
#include <avr/io.h>
#include <avr/sleep.h>

#include "edice_fsm.h"
#include "edice_actions.h"

volatile unsigned char shookCnt = 0; // Shook counter (updated by the interrupt)
unsigned char rn = 0;                // Random number to be displayed

unsigned char sequence[6] = {6, 2, 5, 1, 3, 4};
unsigned char seqIdx      = 0;
unsigned char seqStarted  = 0x0;
unsigned int  seqNext     = 0x0000;

void edice_init(void) {
    // Enable ULP clock
    CCP = CCP_IOREG_gc;
    CLKCTRL.MCLKCTRLA = CLKCTRL_CLKSEL_OSCULP32K_gc;

    // Change  Main clock pre-scaler to 8
    CCP = CCP_IOREG_gc;
    CLKCTRL.MCLKCTRLB = CLKCTRL_PDIV_4X_gc | CLKCTRL_PEN_bm;

    // Configure Timer A with OVF (we do not enable yet)
    TCA0.SINGLE.PER     = 0x2000; // at 32.768kHz, it takes 1 seconds to reach 0x2000 with a prescaller of 4
    TCA0.SINGLE.INTCTRL = TCA_SINGLE_OVF_bm; // Enable OVF interrupt
    TCA0.SINGLE.CTRLB   = TCA_SINGLE_WGMODE_NORMAL_gc;
    TCA0.SINGLE.EVCTRL  &= ~(TCA_SINGLE_CNTEI_bm);
    TCA0.SINGLE.CTRLA   = TCA_SINGLE_CLKSEL_DIV1_gc; // Select DIV=1


    // Enable WDT
    CCP = CCP_IOREG_gc;
    WDT.CTRLA = PERIOD_8KCLK_gc;

    // Outputs (enable output drivers))
    PORTA.DIRSET = PIN1_bm;     // D3, D5
    PORTA.DIRSET = PIN2_bm;     // D4
    PORTA.DIRSET = PIN3_bm;     // D2, D6
    PORTA.DIRSET = PIN7_bm;     // D1, D7

    // Inputs (disable output drivers))
    PORTA.DIRCLR = PIN6_bm;     // Tilt

    // Enable interrupt on both edges of PIN6 (Tilt switch)
    PORTA.PIN6CTRL = PORT_ISC_RISING_gc;

    // Enable global interrupt
    ei();

    // Initialize global variables
    ediceState     = SLEEP;
    prevEdiceState = INIT;
    ediceEvent     = NONE;
}

void edice_reset(void) {
    // Soft reset
    CCP = CCP_IOREG_gc;
    RSTCTRL.SWRR = RSTCTRL_SWRE_bm;
}

void edice_sleep(void) {
    // Go in sleep mode
    SLPCTRL.CTRLA = SLPCTRL_SMODE_PDOWN_gc | SLPCTRL_SEN_bm;
    sleep_cpu();
}

void display_clear(void) {
    PORTA.OUTCLR = PIN1_bm;     // D3, D5
    PORTA.OUTCLR = PIN2_bm;     // D4
    PORTA.OUTCLR = PIN3_bm;     // D2, D6
    PORTA.OUTCLR = PIN7_bm;     // D1, D7
}

void display_number(unsigned char val) {
    val = (val-1) % 6;
    switch (val) {
        case 0: // Display 1 on the dice
            PORTA.OUTCLR = PIN1_bm;     // D3, D5
            PORTA.OUTSET = PIN2_bm;     // D4
            PORTA.OUTCLR = PIN3_bm;     // D2, D6
            PORTA.OUTCLR = PIN7_bm;     // D1, D7
            break;

        case 1: // Display 2 on the dice
            PORTA.OUTSET = PIN1_bm;     // D3, D5
            PORTA.OUTCLR = PIN2_bm;     // D4
            PORTA.OUTCLR = PIN3_bm;     // D2, D6
            PORTA.OUTCLR = PIN7_bm;     // D1, D7
            break;

        case 2: // Display 3 on the dice
            PORTA.OUTSET = PIN1_bm;     // D3, D5
            PORTA.OUTSET = PIN2_bm;     // D4
            PORTA.OUTCLR = PIN3_bm;     // D2, D6
            PORTA.OUTCLR = PIN7_bm;     // D1, D7
            break;

        case 3: // Display 4 on the dice
            PORTA.OUTSET = PIN1_bm;     // D3, D5
            PORTA.OUTCLR = PIN2_bm;     // D4
            PORTA.OUTCLR = PIN3_bm;     // D2, D6
            PORTA.OUTSET = PIN7_bm;     // D1, D7
            break;

        case 4: // Display 5 on the dice
            PORTA.OUTSET = PIN1_bm;     // D3, D5
            PORTA.OUTSET = PIN2_bm;     // D4
            PORTA.OUTCLR = PIN3_bm;     // D2, D6
            PORTA.OUTSET = PIN7_bm;     // D1, D7
            break;

        case 5: // Display 6 on the dice
            PORTA.OUTSET = PIN1_bm;     // D3, D5
            PORTA.OUTCLR = PIN2_bm;     // D4
            PORTA.OUTSET = PIN3_bm;     // D2, D6
            PORTA.OUTSET = PIN7_bm;     // D1, D7
            break;

        default:
            break;
    }
}

void edice_powerdown(void) {
    // Disable the timer
    TCA0.SINGLE.CTRLA    = TCA_SINGLE_CLKSEL_DIV1_gc;
    display_clear();
    edice_sleep();
}

void edice_update_rn(void) {
    // Update the random Number
    di();
    rn += shookCnt;
    ei();
}

void edice_sequence(void) {
    if (prevEdiceState == RNG) {
        // Set the timer to 2 seconds and restart it
        TCA0.SINGLE.PER      = 0x6000;
        TCA0.SINGLE.CTRLESET = TCA_SINGLE_CMD_RESTART_gc | 0x3;
        TCA0.SINGLE.CTRLA    = TCA_SINGLE_CLKSEL_DIV1_gc | TCA_SINGLE_ENABLE_bm;
        seqNext    = 0x0000;
    }

    if (!seqStarted) {
        seqStarted = 0x1;
        seqNext    = 0x0000;
    }

    display_number(sequence[seqIdx]);

    if (TCA0.SINGLE.CNT > seqNext) {
        seqIdx = (seqIdx + 1) % 6;
        seqNext = TCA0.SINGLE.CNT + 0x333 + (TCA0.SINGLE.CNT >> 2);
    }

}

void edice_display_rng(void) {
    if (prevEdiceState == SEQUENCE) {
        // Restart the timer
        TCA0.SINGLE.CTRLA    = TCA_SINGLE_CLKSEL_DIV1_gc;
        TCA0.SINGLE.PER      = 0x8000;
        TCA0.SINGLE.CTRLESET = TCA_SINGLE_CMD_RESTART_gc | 0x3;
        TCA0.SINGLE.CTRLA    = TCA_SINGLE_CLKSEL_DIV1_gc | TCA_SINGLE_ENABLE_bm;

        // Stop the sequence (will force it to restart)
        seqStarted = 0x0;
    }

    // Display the random number
    display_clear();
    for(int i=0;i<200;i++);
    display_number(rn);
    for(int i=0;i<40;i++);
}
