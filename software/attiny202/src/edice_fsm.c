#include <xc.h>
#include <avr/io.h>
#include <avr/wdt.h>

#include "edice_fsm.h"
#include "edice_actions.h"

/*
 * Global variable for the E-Dice state and Events
 * - States are updated by the finite state Machine
 * - Events are update by interrupts (thus we declare the variable volatile)
 */
eState ediceState;
eState prevEdiceState;
volatile eEvent ediceEvent;


void edice_fsm(void) {
    wdt_reset();

    switch(ediceState) {
        case SLEEP:
            // Place the microcontroller in powerdown mode
            edice_powerdown();

            prevEdiceState = ediceState;
            if (ediceEvent == SHOOK) {
                // If the device is shook, go to the RNG state
                edice_clear_event();
                ediceState = RNG;
            }
            break;

        case RNG:
            // Update the random number
            edice_update_rn();

            // Go directly to the SEQUENCE state
            prevEdiceState = ediceState;
            ediceState = SEQUENCE;
            break;

        case SEQUENCE:
            // Trigger the sequence display
            edice_sequence();

            prevEdiceState = ediceState;
            if (ediceEvent == SHOOK) {
                // If the device is shook, go back to the RNG state
                edice_clear_event();
                ediceState = RNG;
                break;
            } else if (ediceEvent == TIMEOUT) {
                // Otherwsie, if the Sequence timer has timed out, go to the
                // DISPLAY state
                edice_clear_event();
                ediceState = DISPLAY;
                break;
            }
            break;

        case DISPLAY:
            edice_display_rng();

            prevEdiceState = ediceState;
            if (ediceEvent == SHOOK) {
                // If the device is shook, go back to the RNG state
                edice_clear_event();
                ediceState = RNG;
                break;
            } else if (ediceEvent == TIMEOUT) {
                // Otherwsie, if the Sequence timer has timed out, go back
                // to the SLEEP state
                edice_clear_event();
                ediceState = SLEEP;
                break;
            }
        break;

        default:
            // Not supposed to ever get here. Something went wrong.
            // Forcing a reset of the device.
            edice_reset();
            break;
    }
}

void edice_clear_event(void) {
    di();
    ediceEvent = NONE;
    ei();
}
