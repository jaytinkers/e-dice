/**
 * ATtiny202 interrupt configuration
 */

#include "edice_fsm.h"
#include "edice_actions.h"

#include <xc.h>
#include <avr/io.h>

void __interrupt(PORTA_PORT_vect_num) tiltInt() {
    // Check for PA6 interrupt flag (device was tilted))
    di();
    if(PORTA.INTFLAGS == PIN6_bm) {
        PORTA.INTFLAGS = PIN6_bm;   // Clear the interrupt
        shookCnt++;
        ediceEvent = SHOOK;
    }
    ei();
    return;
}

void __interrupt(TCA0_OVF_vect_num) timerInt() {
    di();
    TCA0.SINGLE.INTFLAGS = TCA_SINGLE_OVF_bm;   // Clear the interrupt
    ediceEvent = TIMEOUT;
    ei();
    return;

}
