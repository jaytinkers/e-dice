/*
 * E-Dice Finite State Machine Actions
 */

#ifndef EDICE_ACTIONS_H
#define EDICE_ACTIONS_H

extern volatile unsigned char shookCnt;

/*
 * Global initialization of the E-Dice
 */
void edice_init(void);

/*
 * Trigger a soft reset
 */
void edice_reset(void);

/*
 * Put the MCU in Sleep mode
 */
void edice_sleep(void);

/*
 * Clear the disaply (Turn OFF all LEDs)
 */
void display_clear(void);

/*
 * Display a specific number (from 1 to 6)
 */
void display_number(unsigned char val);

/*
 * E-Dice SLEEP state action
 */
void edice_powerdown(void);

/*
 * E-Dice RNG state action
 */
void edice_update_rn(void);

/*
 * E-Dice SEQUENCE state action
 */
void edice_sequence(void);

/*
 * E-Dice DISPLAY state action
 */
void edice_display_rng(void);

#endif
